package br.com.ramires.kravmaga.dto;

import org.jsondoc.core.annotation.ApiObject;
import org.jsondoc.core.annotation.ApiObjectField;

@ApiObject(name = "Delivery", description = "Response object with result of the GET method in kravmaga/calculateDelivery")
public class DeliveryDto {
	
	@ApiObjectField(name = "mapName", description = "Map to use")
	private String mapName;
	
	@ApiObjectField(name = "origin", description = "Origin point")
	private String origin; 
	
	@ApiObjectField(name = "destination", description = "Destination point")
	private String destination;
	
	@ApiObjectField(name = "kmPerLitre", description = "Km/L fuel efficy")
	private Double kmPerLitre;
	
	@ApiObjectField(name = "fuelPrice", description = "Price of fuel")
	private Double fuelPrice;
	
	@ApiObjectField(name = "totalCost", description = "Total cost for this route")
	private double totalCost;
	
	@ApiObjectField(name = "totalDistance", description = "Total distance for this route")
	private double totalDistance;
	
	@ApiObjectField(name = "routesDirection", description = "Direction to go for this route")
	private String routesDirection;
	
	public DeliveryDto() {
	}
	public DeliveryDto(String mapName, String origin, String destination, Double kmPerLitre, Double fuelPrice) {
		this.mapName = mapName;
		this.origin = origin;
		this.destination = destination;
		this.kmPerLitre = kmPerLitre;
		this.fuelPrice = fuelPrice;
	}
	
	public String getMapName() {
		return mapName;
	}
	public void setMapName(String mapName) {
		this.mapName = mapName;
	}
	
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	
	public Double getKmPerLitre() {
		return kmPerLitre;
	}
	public void setKmPerLitre(Double kmPerLitre) {
		this.kmPerLitre = kmPerLitre;
	}
	
	public Double getFuelPrice() {
		return fuelPrice;
	}
	public void setFuelPrice(Double fuelPrice) {
		this.fuelPrice = fuelPrice;
	}
	
	public double getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}
	
	public double getTotalDistance() {
		return totalDistance;
	}
	public void setTotalDistance(double totalDistance) {
		this.totalDistance = totalDistance;
	}
	
	public String getRoutesDirection() {
		return routesDirection;
	}
	public void setRoutesDirection(String routesDirection) {
		this.routesDirection = routesDirection;
	}
	@Override
	public String toString() {
		return "DeliveryDto [mapName=" + mapName + ", origin=" + origin + ", destination=" + destination
				+ ", kmPerLitre=" + kmPerLitre + ", fuelPrice=" + fuelPrice + ", totalCost=" + totalCost
				+ ", totalDistance=" + totalDistance + ", routesDirection=" + routesDirection + "]";
	}
}
