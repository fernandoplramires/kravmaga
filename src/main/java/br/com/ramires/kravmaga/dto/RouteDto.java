package br.com.ramires.kravmaga.dto;

import org.jsondoc.core.annotation.ApiObject;
import org.jsondoc.core.annotation.ApiObjectField;

@ApiObject(name = "Route", description = "Request object to send a route in the Map Object for POST method in kravmaga/map")
public class RouteDto {
	
	@ApiObjectField(name = "origin", description = "Origin point name")
	private String origin;
	
	@ApiObjectField(name = "destination", description = "Destination point name")
	private String destination;
	
	@ApiObjectField(name = "distance", description = "Distance of origin and destination points")
	private double distance;
	
	public RouteDto() {
	}
    public RouteDto(String origin, String destination, double distance) {
        this.origin = origin;
        this.destination = destination;
        this.distance = distance;
    }
	
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	
	public double getDistance() {
		return distance;
	}
	public void setDistance(double distance) {
		this.distance = distance;
	}
	
	@Override
	public String toString() {
		return "RouteDto=[origin=" + origin + ", destination=" + destination + ", distance=" + distance + "]";
	}
}
