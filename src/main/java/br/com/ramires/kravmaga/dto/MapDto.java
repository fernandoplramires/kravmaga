package br.com.ramires.kravmaga.dto;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.jsondoc.core.annotation.ApiObject;
import org.jsondoc.core.annotation.ApiObjectField;

@ApiObject(name = "Map", description = "Request object to send a Map Route in the POST method in kravmaga/map")
public class MapDto {
	
	@ApiObjectField(name = "name", description = "Name of Map")
	private String name;
	
	@ApiObjectField(name = "routes", description = "List of the Routes")
	private Set<RouteDto> routes = new HashSet<>();
	
	public MapDto() {
	}
	public MapDto(String name, Set<RouteDto> routes) {
		this.name = name;
		this.routes = routes;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public void setRoutes(Set<RouteDto> routes) {
		this.routes = routes;
	}
	public Set<RouteDto> getRoutes() {
		return Collections.unmodifiableSet(routes);
	}
	
	@Override
	public String toString() {
		return "MapDto [name=" + name + ", routes=" + routes + "]";
	}
}
