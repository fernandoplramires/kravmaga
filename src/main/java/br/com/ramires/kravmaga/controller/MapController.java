package br.com.ramires.kravmaga.controller;

import org.jsondoc.core.annotation.Api;
import org.jsondoc.core.annotation.ApiBodyObject;
import org.jsondoc.core.annotation.ApiMethod;
import org.jsondoc.core.annotation.ApiResponseObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import br.com.ramires.kravmaga.dto.MapDto;
import br.com.ramires.kravmaga.exception.MapDefaultException;
import br.com.ramires.kravmaga.exception.MapValidatorException;
import br.com.ramires.kravmaga.service.MapService;

@RestController
@RequestMapping("/kravmaga")
@Api(description = "The Map Controller to send a new map routes", name = "Map Services")
public class MapController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MapController.class);
	
	@Autowired
	MapService service;
	
	@ApiMethod(description = "Send a map with routes")
    @RequestMapping(value="/map", 
    		method = RequestMethod.POST, 
    		consumes = MediaType.APPLICATION_JSON_VALUE,
    		produces = MediaType.APPLICATION_JSON_VALUE)
	public @ApiResponseObject @ResponseBody ResponseEntity<Void> receiveMap(
		@ApiBodyObject @RequestBody MapDto mapDtoRequest) {
		
		LOGGER.debug("MapController->receiveMap->begin()");
		LOGGER.warn("MapController->receiveMap-> Request received, run the service");
		LOGGER.debug(String.format("MapController->receiveMap-> Request: %s", mapDtoRequest));
    	
		service.run(mapDtoRequest);
		
		HttpHeaders headers = new HttpHeaders();
		ResponseEntity<Void> responseEntity = new ResponseEntity<Void>(headers, HttpStatus.CREATED);
		
		LOGGER.warn("MapController->receiveMap-> Response generated, send back");
		LOGGER.debug(String.format("MapController->receiveMap-> Response: %s", responseEntity));
		LOGGER.debug("MapController->receiveMap->end()");
		return responseEntity;
	}
    
	@ExceptionHandler
    public ResponseEntity<String> handleMapDefaultException(MapDefaultException e)  {
    	
		LOGGER.error(String.format("MapController->handleMapDefaultException-> Received Exception with the message: %s", e));
    	return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_ACCEPTABLE);
    }
	
	@ExceptionHandler
    public ResponseEntity<String> handleMapValidatorException(MapValidatorException e)  {
    	
		LOGGER.error(String.format("MapController->handleMapValidatorException-> Received Exception with the message: %s", e));
    	return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
