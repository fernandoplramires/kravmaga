package br.com.ramires.kravmaga.controller;

import org.jsondoc.core.annotation.Api;
import org.jsondoc.core.annotation.ApiMethod;
import org.jsondoc.core.annotation.ApiQueryParam;
import org.jsondoc.core.annotation.ApiResponseObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import br.com.ramires.kravmaga.dto.DeliveryDto;
import br.com.ramires.kravmaga.service.DeliveryService;
import br.com.ramires.kravmaga.exception.DeliveryDefaultException;
import br.com.ramires.kravmaga.exception.DeliveryRepositoryException;
import br.com.ramires.kravmaga.exception.DeliveryValidatorException;

@RestController
@RequestMapping("/kravmaga")
@Api(name = "Delivery Services", description = "The Delivery Controller to get a Delivery results")
public class DeliveryController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DeliveryController.class);
	
	@Autowired
	private DeliveryService service;
	
	@ApiMethod(description = "Return a Delivery object with results of the total cost and the best path route")
	@RequestMapping(value = "/calculateDelivery", 
			method 	= 	RequestMethod.GET,  
			produces = 	MediaType.APPLICATION_JSON_VALUE)
	public @ApiResponseObject @ResponseBody ResponseEntity<DeliveryDto> calculateDelivery(
		@ApiQueryParam(name = "mapName",		description = "Map route name") 			@RequestParam("mapName") 		String mapName,
		@ApiQueryParam(name = "origin", 		description = "Origin point name") 			@RequestParam("origin") 		String origin,
		@ApiQueryParam(name = "destination", 	description = "Destination point name") 	@RequestParam("destination") 	String destination,
		@ApiQueryParam(name = "kmPerLitre", 	description = "Fuel efficiency in KM/L") 	@RequestParam("kmPerLitre") 	double kmPerLitre,
		@ApiQueryParam(name = "fuelPrice", 		description = "Price of the fuel") 			@RequestParam("fuelPrice") 		double fuelPrice) {
		
		LOGGER.debug("DeliveryController->calculateDelivery->begin()");
		
		DeliveryDto deliveryDtoRequest = new DeliveryDto(mapName,origin,destination,kmPerLitre,fuelPrice);
		
		LOGGER.warn("DeliveryController->calculateDelivery-> Request received, run the service");
		LOGGER.debug(String.format("DeliveryController->calculateDelivery-> Request: %s", deliveryDtoRequest));
		
		DeliveryDto deliveryDtoResponse = service.run(deliveryDtoRequest);
		
		LOGGER.warn("DeliveryController->calculateDelivery-> Response generated, send back");
		LOGGER.debug(String.format("DeliveryController->calculateDelivery-> Response: %s", deliveryDtoResponse));
		LOGGER.debug("DeliveryController->calculateDelivery->end()");
		return new ResponseEntity<DeliveryDto>(deliveryDtoResponse, HttpStatus.OK);
	}
	
	@ExceptionHandler
	public ResponseEntity<String> handleDeliveryDefaultException(DeliveryDefaultException e)  {
		
		LOGGER.error(String.format("DeliveryController->handleDeliveryDefaultException-> Received Exception with the message: %s", e.getMessage()));
		return new ResponseEntity<String>(e.getMessage(), HttpStatus.CONFLICT);
	}
	
	@ExceptionHandler
	public ResponseEntity<String> handleDeliveryRepositoryException(DeliveryRepositoryException e)  {
		
		LOGGER.error(String.format("DeliveryController->handleDeliveryRepositoryException-> Received Exception with the message: %s", e.getMessage()));
		return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler
	public ResponseEntity<String> handleDeliveryValidatorException(DeliveryValidatorException e)  {
		
		LOGGER.error(String.format("DeliveryController->handleDeliveryValidatorException-> Received Exception with the message: %s", e.getMessage()));
		return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
	}
}
