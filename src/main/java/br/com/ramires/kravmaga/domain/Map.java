package br.com.ramires.kravmaga.domain;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

//import org.neo4j.ogm.annotation.NodeEntity;
//import org.neo4j.ogm.annotation.Property;
//import org.neo4j.ogm.annotation.Relationship;
import org.neo4j.ogm.annotation.Transient;

@Transient
public class Map extends AbstractEntity {
	
	private String name;
	private Set<Route> routes = new HashSet<>();

	public Map() {
	}
	public Map(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public void addRoute(Route routeParameter) {
		this.routes.add(routeParameter);
	}
	public Set<Route> getRoutes() {
		return Collections.unmodifiableSet(routes);
	}
    public boolean hasRoute(Route route) {
    	for (Route routeInterator : routes) {
    		if (route.getMapName() == routeInterator.getMapName()) {
    			if (route.getRouteName() == routeInterator.getRouteName()) {
    				if (route.getDistance() == routeInterator.getDistance()) {
    					return true;
    				}
    			}
    		}
    	}
        return false;
    }
	
	@Override
	public String toString() {
		return "Map[id=" + super.getId() + ", name=" + name + ", routes=" + routes + "]";
	}
}