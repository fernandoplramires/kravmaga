package br.com.ramires.kravmaga.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Property;

@NodeEntity(label = "POINT")
public class Point extends AbstractEntity {
	
	@Property(name="name")
	private String name;
    
	@Property(name="map")
	private String map;
	
	private List<Route> routes = new ArrayList<>();
	
	public Point() {
	}
	public Point(String name, String mapName) {
		this.name = name;
		this.map = mapName;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getMapName() {
		return map;
	}
	public void setMapName(String mapName) {
		this.map = mapName;
	}
	
	public void addRoute(Route route) {
		this.routes.add(route);
	}
	public List<Route> getRoutes() {
		return Collections.unmodifiableList(routes);
	}
    public boolean hasRoute(Route route) {
    	for (Route routeInterator : routes) {
    		if (route.getMapName() == routeInterator.getMapName()) {
    			if (route.getRouteName() == routeInterator.getRouteName()) {
    				if (route.getDistance() == routeInterator.getDistance()) {
    					return true;
    				}
    			}
    		}
    	}
        return false;
    }
	
	@Override
	public String toString() {
		return "Pointer[id=" + super.getId() + ", name=" + name + ", map=" + map + "]";
	}
}