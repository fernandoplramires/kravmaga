package br.com.ramires.kravmaga.domain;

import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.Property;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;

@RelationshipEntity
public class Route extends AbstractEntity {
	
	@Property(name="name")
	private String name;
	
	@Property(name="map")
	private String map;
	
	@Property(name="distance")
	private double distance;
	
	@StartNode
	private Point origin;
	
	@EndNode
	private Point destination;
	
	public Route() {
		super();
	}
    public Route(String mapName, Point origin, Point destination, double distance) {
    	super();
    	this.name = origin.getName() + "-" + destination.getName();
    	this.map = mapName;
    	this.origin = origin;
        this.destination = destination;
        this.distance = distance;
    }
    public Route(String routeName, String mapName, Point origin, Point destination, double distance) {
    	super();
    	this.name = routeName;
    	this.map = mapName;
        this.origin = origin;
        this.destination = destination;
        this.distance = distance;
    }
	
	public String getRouteName() {
		return name;
	}
	public void setRouteName(String routeName) {
		this.name = routeName;
	}
	
	public String getMapName() {
		return map;
	}
	public void setMapName(String mapName) {
		this.map = mapName;
	}
	
	public Point getOrigin() {
		return origin;
	}
	public void setOrigin(Point origin) {
		this.origin = origin;
	}
	public Point getDestination() {
		return destination;
	}
	public void setDestination(Point destination) {
		this.destination = destination;
	}
	
	public double getDistance() {
		return distance;
	}
	public void setDistance(double distance) {
		this.distance = distance;
	}
	
	public String toDebugString() {
		return "Route[name=" + name + ", map=" + map + ", origin=" + origin.getName() 
			 	+ ", destination=" + destination.getName() + ", distance=" + distance + "]";
	}
	
	@Override
	public String toString() {
		return "Route[id=" + super.getId() + ", name=" + name + ", map=" + map 
				+ ", origin=" + origin.getName() + ", destination=" + destination.getName() + ", distance=" + distance + "]";
	}
}
