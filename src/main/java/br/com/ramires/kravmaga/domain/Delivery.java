package br.com.ramires.kravmaga.domain;

import org.neo4j.ogm.annotation.Transient;

@Transient
public class Delivery extends AbstractEntity {
	
	private String mapName;
	private String origin; 
	private String destination;
	private double kmPerLitre;
	private double fuelPrice;
	private double totalCost;
	private double totalDistance;
	private String routesDirection;
	
	public Delivery() {
	}
	
	public String getMapName() {
		return mapName;
	}
	public void setMapName(String mapName) {
		this.mapName = mapName;
	}
	
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	
	public Double getKmPerLitre() {
		return kmPerLitre;
	}
	public void setKmPerLitre(Double kmPerLitre) {
		this.kmPerLitre = kmPerLitre;
	}
	
	public Double getFuelPrice() {
		return fuelPrice;
	}
	public void setFuelPrice(Double fuelPrice) {
		this.fuelPrice = fuelPrice;
	}
	
	public double getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}
	public void calculateTotalCost() {
		totalCost = (fuelPrice * totalDistance) / kmPerLitre;
	}
	
	public double getTotalDistance() {
		return totalDistance;
	}
	public void setTotalDistance(double totalDistance) {
		this.totalDistance = totalDistance;
	}
	
	public String getRoutesDirection() {
		return routesDirection;
	}
	public void setRoutesDirection(String routesDirection) {
		this.routesDirection = routesDirection;
	}

	@Override
	public String toString() {
		return "Delivery [id=" + super.getId() + ", mapName=" + mapName + ", origin=" + origin 
				+ ", destination=" + destination + ", kmPerLitre=" + kmPerLitre + ", fuelPrice=" + fuelPrice 
				+ ", totalCost=" + totalCost + ", totalDistance=" + totalDistance + ", routesDirection=" + routesDirection + "]";
	}
}
