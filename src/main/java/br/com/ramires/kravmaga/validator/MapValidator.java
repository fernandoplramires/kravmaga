package br.com.ramires.kravmaga.validator;

import br.com.ramires.kravmaga.dto.MapDto;

public interface MapValidator {
	
	public void validate(MapDto mapDto);
}
