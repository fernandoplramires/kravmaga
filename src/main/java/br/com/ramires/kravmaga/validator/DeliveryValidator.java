package br.com.ramires.kravmaga.validator;

import br.com.ramires.kravmaga.dto.DeliveryDto;

public interface DeliveryValidator {
	
	public void validate(DeliveryDto deliveryDto);
}
