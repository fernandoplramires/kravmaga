package br.com.ramires.kravmaga.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import br.com.ramires.kravmaga.dto.MapDto;
import br.com.ramires.kravmaga.dto.RouteDto;
import br.com.ramires.kravmaga.exception.MapValidatorException;

@Component
public class MapValidatorImpl implements MapValidator {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MapValidatorImpl.class);
	
	public void validate(MapDto mapDto) {
		
		LOGGER.debug("MapValidatorImpl->validate->begin()");
		
		//Map context validate.
		LOGGER.debug("MapValidatorImpl->validate-> (Check) Map context");
		if (mapDto == null) {
			throw new MapValidatorException("Map file is empty.");
		}
		
		//Map mandatory fields.
		LOGGER.debug("MapValidatorImpl->validate-> (Check) Map mandatory field");
		if (mapDto.getName() == null || mapDto.getRoutes() == null) {
			throw new MapValidatorException("Map mandatory fields are not correct inputed.");
		}
		
		//Name of map validate.
		LOGGER.debug("MapValidatorImpl->validate-> (Check) Name of map");
		if (mapDto.getName().equals("")) {	
			throw new MapValidatorException("field name map is empty.");
		}
		
		//Name of map with characters invalid validate.
		LOGGER.debug("MapValidatorImpl->validate-> (Check) Name of map with invalid characters");
		if (containsIllegals(mapDto.getName())) {
			throw new MapValidatorException("field name map with invalid characters");
		}
		
		//List of routes of map validate.
		LOGGER.debug("MapValidatorImpl->validate-> (Check) List of routes of map");
		if (mapDto.getRoutes().isEmpty()) {  
			throw new MapValidatorException("field routes is empty.");
		}
		
		//For each route validate.
		LOGGER.debug("MapValidatorImpl->execute-> -------------------------------------------------------------------------------");
		LOGGER.debug("MapValidatorImpl->validate-> For each route");
		LOGGER.debug("MapValidatorImpl->execute-> -------------------------------------------------------------------------------");
		for (RouteDto routeDto : mapDto.getRoutes()) {
			
			//Mandatory fields of route validate.
			LOGGER.debug("MapValidatorImpl->validate-> (Check) Mandatory fields of route");
			if (routeDto.getOrigin() == null || routeDto.getDestination() == null) {
				throw new MapValidatorException("fields mandatory origin and destination for route are not correct inputed.");
			}
			
			//Origin and destination of route validate.
			LOGGER.debug("MapValidatorImpl->validate-> (Check) Origin and destination of route");
			if (routeDto.getOrigin().isEmpty() || routeDto.getDestination().isEmpty()) {
				throw new MapValidatorException("field origin or destination for route is empty.");
			}
			
			//Origin and destination of route with characters invalid validate.
			LOGGER.debug("MapValidatorImpl->validate-> (Check) Origin and destination of route with charackters invalid");
			if (containsIllegals(routeDto.getOrigin()) || containsIllegals(routeDto.getDestination())) {
				throw new MapValidatorException("field origin or destination for route with invalid characters");
			}
			
			//Distance of route validate.
			LOGGER.debug("MapValidatorImpl->validate-> (Check) Distance of route");
			if (routeDto.getDistance() < 0) {
				throw new MapValidatorException("field distance of route is not a valid distance.");
			}
		}
		LOGGER.debug("MapValidatorImpl->execute-> -------------------------------------------------------------------------------");
		
		LOGGER.debug("MapValidatorImpl->validate->end()");
	}
	
	public boolean containsIllegals(String toExamine) {
	    Pattern pattern = Pattern.compile("^[a-zA-Z0-9]*$");
	    Matcher matcher = pattern.matcher(toExamine);
	    return !matcher.find();
	}
}
