package br.com.ramires.kravmaga.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import br.com.ramires.kravmaga.dto.DeliveryDto;
import br.com.ramires.kravmaga.exception.DeliveryValidatorException;
import br.com.ramires.kravmaga.exception.MapValidatorException;

@Component
public class DeliveryValidatorImpl implements DeliveryValidator {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DeliveryValidatorImpl.class);
	
	public void validate(DeliveryDto deliveryDto) {
		
		LOGGER.debug("DeliveryValidatorImpl->validate->begin()");
		
		//Delivery context validate.
		LOGGER.debug("DeliveryValidatorImpl->validate-> (Check) Delivery context validate");
		if (deliveryDto == null) {
			throw new DeliveryValidatorException("Delivery file is empty.");
		}
		
		//Delivery mandatory fields.
		LOGGER.debug("DeliveryValidatorImpl->validate-> (Check) Delivery mandatory fields");
		if (deliveryDto.getOrigin() == null    || deliveryDto.getDestination() == null ||
				deliveryDto.getFuelPrice() == null || deliveryDto.getKmPerLitre() == null) {
			throw new DeliveryValidatorException("Delivery mandatory fields are not correct inputed.");
		}
		
		//ORigin of delivery validate.
		LOGGER.debug("DeliveryValidatorImpl->validate-> (Check) KmPerLitre of delivery");
		if (deliveryDto.getOrigin().equals("")) {
			throw new DeliveryValidatorException("field origin is empty.");
		}
		
		//Destination of delivery validate.
		LOGGER.debug("DeliveryValidatorImpl->validate-> (Check) Destination of delivery");
		if (deliveryDto.getDestination().equals("")) {
			throw new DeliveryValidatorException("field destination is empty.");
		}
		
		//Origin and destination of route with characters invalid validate.
		LOGGER.debug("DeliveryValidatorImpl->validate-> (Check) Origin and destination with charackters invalid");
		if (containsIllegals(deliveryDto.getOrigin()) == true || containsIllegals(deliveryDto.getDestination()) == true) {
			throw new DeliveryValidatorException("field origin or destination with invalid characters");
		}
		
		//Fuel price of delivery validate.
		LOGGER.debug("DeliveryValidatorImpl->validate-> (Check) Fuel price of delivery");
		if (deliveryDto.getFuelPrice() <= 0) {
			throw new DeliveryValidatorException("field fuelPrice is not contain a valid value.");
		}
		
		//Km per Litre of delivery validate.
		LOGGER.debug("DeliveryValidatorImpl->validate-> (Check) Km per Litre of delivery");
		if (deliveryDto.getKmPerLitre() <= 0) { 
			throw new DeliveryValidatorException("field kmPerLitre is not contain a valid value.");
		}
		
		LOGGER.debug("DeliveryValidatorImpl->validate->end()");
	}
	
	public boolean containsIllegals(String toExamine) {
	    Pattern pattern = Pattern.compile("^[a-zA-Z0-9]*$");
	    Matcher matcher = pattern.matcher(toExamine);
	    return !matcher.find();
	}
}
