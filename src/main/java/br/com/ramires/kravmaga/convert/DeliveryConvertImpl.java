package br.com.ramires.kravmaga.convert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import br.com.ramires.kravmaga.domain.Delivery;
import br.com.ramires.kravmaga.dto.DeliveryDto;

@Component
public class DeliveryConvertImpl implements DeliveryConvert {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DeliveryConvertImpl.class);
	
	public DeliveryDto convertToDto(final Delivery deliveryDomain) {
		
		LOGGER.debug("DeliveryConvertImpl->convertToDto->begin()");
		DeliveryDto deliveryDto = new DeliveryDto();
		
		deliveryDto.setMapName(deliveryDomain.getMapName());
		deliveryDto.setOrigin(deliveryDomain.getOrigin());
		deliveryDto.setDestination(deliveryDomain.getDestination());
		deliveryDto.setKmPerLitre(deliveryDomain.getKmPerLitre());
		deliveryDto.setFuelPrice(deliveryDomain.getFuelPrice());
		deliveryDto.setTotalCost(deliveryDomain.getTotalCost());
		deliveryDto.setTotalDistance(deliveryDomain.getTotalDistance());
		deliveryDto.setRoutesDirection(deliveryDomain.getRoutesDirection());
		
		LOGGER.debug("DeliveryConvertImpl->convertToDto->begin()");
		return deliveryDto;
	}
	
	public Delivery convertToDomain(final DeliveryDto deliveryDto) {
		
		LOGGER.debug("DeliveryConvertImpl->convertToDomain->begin()");
		Delivery deliveryDomain = new Delivery();
		
		deliveryDomain.setMapName(deliveryDto.getMapName());
		deliveryDomain.setOrigin(deliveryDto.getOrigin());
		deliveryDomain.setDestination(deliveryDto.getDestination());
		deliveryDomain.setKmPerLitre(deliveryDto.getKmPerLitre());
		deliveryDomain.setFuelPrice(deliveryDto.getFuelPrice());
		deliveryDomain.setTotalCost(deliveryDto.getTotalCost());
		deliveryDomain.setTotalDistance(deliveryDto.getTotalDistance());
		deliveryDomain.setRoutesDirection(deliveryDto.getRoutesDirection());
		
		LOGGER.debug("DeliveryConvertImpl->convertToDomain->begin()");
		return deliveryDomain;
	}
}
