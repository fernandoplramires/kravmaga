package br.com.ramires.kravmaga.convert;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import br.com.ramires.kravmaga.controller.MapController;
import br.com.ramires.kravmaga.domain.Map;
import br.com.ramires.kravmaga.domain.Point;
import br.com.ramires.kravmaga.domain.Route;
import br.com.ramires.kravmaga.dto.MapDto;
import br.com.ramires.kravmaga.dto.RouteDto;

@Component
public class MapConvertImpl implements MapConvert {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MapConvertImpl.class);
	
	public MapDto convertToDto(final Map mapDomain) {
		
		LOGGER.debug("MapConvertImpl->convertToDto->begin()");
		MapDto mapDto = new MapDto();
		mapDto.setName(mapDomain.getName());
		
		Set<RouteDto> routesDto = new HashSet<>();
		for (Route route : mapDomain.getRoutes()) {
			
			RouteDto routeDto = new RouteDto();	
			routeDto.setOrigin(route.getOrigin().getName());
			routeDto.setDestination(route.getDestination().getName());
			routeDto.setDistance(route.getDistance());
			routesDto.add(routeDto);
		}
		mapDto.setRoutes(routesDto);
		
		LOGGER.debug("MapConvertImpl->convertToDto->end()");
		return mapDto;
	}
	
	public Map convertToDomain(final MapDto mapDto) {
		
		LOGGER.debug("MapConvertImpl->convertToDomain->begin()");
		Map mapDomain = new Map();
		mapDomain.setName(mapDto.getName().trim());
		
		for (RouteDto routeDto : mapDto.getRoutes()) {
			
			Route route = new Route();
			String routeName = routeDto.getOrigin().trim() + "-" + routeDto.getDestination().trim();
			route.setRouteName(routeName);
			
			route.setMapName(mapDomain.getName().trim());
			
			Point origin = new Point(routeDto.getOrigin().trim(), route.getMapName());
			route.setOrigin(origin);
			
			Point destination = new Point(routeDto.getDestination().trim(), route.getMapName());
			route.setDestination(destination);
			
			route.setDistance(routeDto.getDistance());
			mapDomain.addRoute(route);
		}
		
		LOGGER.debug("MapConvertImpl->convertToDomain->end()");
		return mapDomain;
	}
}
