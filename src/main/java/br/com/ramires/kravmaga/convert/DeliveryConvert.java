package br.com.ramires.kravmaga.convert;

import br.com.ramires.kravmaga.domain.Delivery;
import br.com.ramires.kravmaga.dto.DeliveryDto;

public interface DeliveryConvert {
	
	public DeliveryDto convertToDto(final Delivery deliveryDomain);
	public Delivery convertToDomain(final DeliveryDto deliveryDto);
}
