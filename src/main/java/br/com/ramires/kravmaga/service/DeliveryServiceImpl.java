package br.com.ramires.kravmaga.service;

import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ramires.kravmaga.convert.DeliveryConvert;
import br.com.ramires.kravmaga.domain.Delivery;
import br.com.ramires.kravmaga.domain.Point;
import br.com.ramires.kravmaga.domain.Route;
import br.com.ramires.kravmaga.dto.DeliveryDto;
import br.com.ramires.kravmaga.exception.DeliveryRepositoryException;
import br.com.ramires.kravmaga.repository.PointRepository;
import br.com.ramires.kravmaga.repository.RouteRepository;
import br.com.ramires.kravmaga.service.DeliveryService;
import br.com.ramires.kravmaga.validator.DeliveryValidator;

@Service
public class DeliveryServiceImpl implements DeliveryService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DeliveryServiceImpl.class);
	
	@Autowired
	DeliveryConvert deliveryConvert;
	
	@Autowired
	DeliveryValidator deliveryValidator;
	
	@Autowired
	RouteRepository routeRepository;
	
	@Autowired
	PointRepository pointRepository;
	
	public DeliveryDto run(DeliveryDto deliveryDto) {
		
		LOGGER.debug("DeliveryServiceImpl->run->begin()");
		
		Delivery deliveryDomain = before(deliveryDto);
		deliveryDomain = execute(deliveryDomain);
		deliveryDto = after(deliveryDomain);
		
		LOGGER.debug("DeliveryServiceImpl->run->end()");
		return deliveryDto;
	}
	
	public Delivery before(final DeliveryDto deliveryDto) {
		
		LOGGER.debug("DeliveryServiceImpl->before->begin()");
		
		LOGGER.warn("DeliveryServiceImpl->before-> Validate Data Transfer Object <DeliveryDto>");
		deliveryValidator.validate(deliveryDto);
		
		LOGGER.warn("DeliveryServiceImpl->before-> Convert Data Transfer Object <DeliveryDto> to Domain Object <Delivery>");
		Delivery deliveryDomain = deliveryConvert.convertToDomain(deliveryDto);
		
		LOGGER.debug("DeliveryServiceImpl->before->end()");
		return deliveryDomain;
	}
	
	public Delivery execute(Delivery deliveryDomain) {
		
		LOGGER.debug("DeliveryServiceImpl->execute->begin()");
		
		String originName = deliveryDomain.getOrigin();
		String destinationName = deliveryDomain.getDestination();
		String mapName = deliveryDomain.getMapName();
		
		LOGGER.warn("DeliveryServiceImpl->execute-> ----------------------------------------------------------------------------");
		LOGGER.warn("DeliveryServiceImpl->execute-> (Origin) Try to find this point for this route map...");
		LOGGER.warn("DeliveryServiceImpl->execute-> ----------------------------------------------------------------------------");
		Point origin = pointRepository.findByNameAndMap(originName, mapName);
		if (origin != null) {
			
			LOGGER.warn("DeliveryServiceImpl->execute-> (Origin) Found this point in the registers!");
			LOGGER.debug(String.format("DeliveryServiceImpl->execute-> (Origin) Found this point in the registers: %s", origin.toString()));
		} else {
			
			LOGGER.warn("DeliveryServiceImpl->execute-> (Origin) Origin point not exists in this map routes, raise exception!");
			throw new DeliveryRepositoryException("Origin point not exists in this map routes");
		}
		
		LOGGER.warn("DeliveryServiceImpl->execute-> ----------------------------------------------------------------------------");
		LOGGER.warn("DeliveryServiceImpl->execute-> (Destination) Try to find this point for this route map...");
		LOGGER.warn("DeliveryServiceImpl->execute-> ----------------------------------------------------------------------------");
		Point destination = pointRepository.findByNameAndMap(destinationName, mapName);
		if (destination != null) {
			
			LOGGER.warn("DeliveryServiceImpl->execute-> (Destination) Found this point in the registers!");
			LOGGER.debug(String.format("DeliveryServiceImpl->execute-> (Destination) Found this point in the registers: %s", destination.toString()));
		} else {
			
			LOGGER.warn("DeliveryServiceImpl->execute-> (Destination) Destination point not exists in this map routes, raise exception!");
			throw new DeliveryRepositoryException("Destination point not exists in this map routes");
		}
		
		LOGGER.warn("DeliveryServiceImpl->execute-> ----------------------------------------------------------------------------");
		LOGGER.warn("DeliveryServiceImpl->execute-> (Route) Try to find for a shortest distance path by Dijkstra Query...");
		LOGGER.warn("DeliveryServiceImpl->execute-> ----------------------------------------------------------------------------");
		Iterable<java.util.Map<String, Object>> results = routeRepository.getShortestWeightedPathByDijkstra(originName, destinationName, mapName);
		if (results.iterator().hasNext()) {
			
			LOGGER.warn("DeliveryServiceImpl->execute-> (Route) Found a shortest distance path route for this points and map!");
			for (java.util.Map<String, Object> row : results) {
				for (java.util.Map.Entry<String, Object> entryDetail : row.entrySet()) {
					
					LOGGER.debug(String.format("DeliveryServiceImpl->execute-> Route results value: key=[%s] value=[%s]", entryDetail.getKey(), entryDetail.getValue().toString()));
					
					//Get the shortest path.
					if (entryDetail.getKey().equals("shortestPath")) {
						
						LOGGER.warn("DeliveryServiceImpl->execute-> (Route) Formatter the shortest route...");
						String shortestRoutePath = new String("");
						@SuppressWarnings("unchecked")
						List< java.util.Map<String, Object> > routeDetail = (List<java.util.Map<String, Object>>) entryDetail.getValue();
						for(int register = 0; register < routeDetail.size(); register++ ) {
							
							//Formatter the shortest route.
							Object routeName = (Object) routeDetail.get(register).get("name");
							if (register == 0) {
								shortestRoutePath = shortestRoutePath + routeName.toString();
							} else {
								String auxString = routeName.toString();
								auxString = auxString.substring(auxString.indexOf("-") + 1, auxString.length());
								shortestRoutePath = shortestRoutePath + "-" + auxString;
							}
						}
						
						LOGGER.warn("DeliveryServiceImpl->execute-> (Route) Shortest route formated!");
						LOGGER.debug(String.format("DeliveryServiceImpl->execute-> (Route) Shortest route format: %s", shortestRoutePath));
						deliveryDomain.setRoutesDirection(shortestRoutePath);
					}
					
					//Get total distance.
					if (entryDetail.getKey().equals("totalDistance")) {
						
						LOGGER.warn("DeliveryServiceImpl->execute-> (Route) Get the total distance and calculate the total cost...");
						double totalDistance = (double) entryDetail.getValue();
						deliveryDomain.setTotalDistance(totalDistance);
						deliveryDomain.calculateTotalCost();						
						LOGGER.debug(String.format("DeliveryServiceImpl->execute-> (Route) Total distance: %s", totalDistance));
						LOGGER.debug(String.format("DeliveryServiceImpl->execute-> (Route) Total cost: %s", deliveryDomain.getTotalCost()));
					}
				}
			}
		} else {
			
			LOGGER.warn("DeliveryServiceImpl->execute-> (Route) No route existis in this points and map!");
			deliveryDomain.setRoutesDirection("NO ROUTES FOR THIS ORIGIN AND DESTINATION IN THIS MAP");
		}
		LOGGER.warn("DeliveryServiceImpl->execute-> ----------------------------------------------------------------------------");
		
		LOGGER.debug("execute->end()");
		return deliveryDomain;
	}
	
	public DeliveryDto after(final Delivery deliveryDomain) {
		
		LOGGER.debug("DeliveryServiceImpl->after->begin()");
		
		LOGGER.warn("DeliveryServiceImpl->after-> Convert Domain Object <Delivert> to Data Transfer Object <DeliveryDto>");
		DeliveryDto deliveryDto = deliveryConvert.convertToDto(deliveryDomain);
		
		LOGGER.debug("DeliveryServiceImpl->after->end()");
		return deliveryDto;
	}
}
