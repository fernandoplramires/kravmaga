package br.com.ramires.kravmaga.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ramires.kravmaga.controller.MapController;
import br.com.ramires.kravmaga.convert.MapConvert;
import br.com.ramires.kravmaga.domain.Map;
import br.com.ramires.kravmaga.domain.Point;
import br.com.ramires.kravmaga.domain.Route;
import br.com.ramires.kravmaga.dto.MapDto;
import br.com.ramires.kravmaga.repository.PointRepository;
import br.com.ramires.kravmaga.repository.RouteRepository;
import br.com.ramires.kravmaga.validator.MapValidator;

@Service
public class MapServiceImpl implements MapService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MapController.class);
	
	@Autowired
	MapConvert mapConvert;
	
	@Autowired
	MapValidator mapValidator;
	
	@Autowired
	RouteRepository routeRepository;

	@Autowired
	PointRepository pointRepository;

	public void run(MapDto mapDto) {
		
		LOGGER.debug("MapServiceImpl->run->begin()");
		
		Map mapDomain = before(mapDto);	
		mapDomain = execute(mapDomain);
		mapDto = after(mapDomain);
		
		LOGGER.debug("MapServiceImpl->run->end()");
	}
	
	private Map before(final MapDto mapDto) {
		
		LOGGER.debug("MapServiceImpl->before->begin()");
		
		LOGGER.warn("MapServiceImpl->before-> Validate Data Transfer Object <DeliveryDto>");
		mapValidator.validate(mapDto);
		
		LOGGER.warn("MapServiceImpl->before-> Convert Data Transfer Object <MapDto> to Domain Object <Map>");
		Map mapDomain = mapConvert.convertToDomain(mapDto);
		
		LOGGER.debug("MapServiceImpl->before->end()");
		return mapDomain;
	}
	
	private Map execute(Map mapDomain) {
		
		LOGGER.debug("MapServiceImpl->execute->begin()");
		LOGGER.warn("MapServiceImpl->execute-> ---------------------------------------------------------------------------------");
		LOGGER.warn("MapServiceImpl->execute-> For each routes ...");
		LOGGER.warn("MapServiceImpl->execute-> ---------------------------------------------------------------------------------");
		for (Route route: mapDomain.getRoutes()) {
			
			LOGGER.debug(String.format("MapServiceImpl->execute-> *** %s ***", route.toDebugString()));
			
			//Search for the destination point of the route, if not exists, store in the database.
			LOGGER.warn("MapServiceImpl->execute-> (Destination) Try to find this point for this route map...");
			Point destination = pointRepository.findByNameAndMap(route.getDestination().getName(), route.getMapName());
			if (destination == null) {
				
				destination = new Point(route.getDestination().getName(), route.getMapName());
				LOGGER.warn("MapServiceImpl->execute-> (Destination) This is a new point to save!");
				LOGGER.debug(String.format("MapServiceImpl->execute-> (Destination) This is a new point to save: %s", destination.toString()));
				pointRepository.save(destination);
			} else {
				
				LOGGER.warn("MapServiceImpl->execute-> (Destination) Found this point in the registers!");
				LOGGER.debug(String.format("MapServiceImpl->execute-> (Destination) Found this point in the registers: %s", destination.toString()));
			}
			
			//Search for the origin point of the route, if not exists, store in the database.
			LOGGER.warn("MapServiceImpl->execute-> (Origin) Try to find this point for this route map...");
			Point origin = pointRepository.findByNameAndMap(route.getOrigin().getName(), route.getMapName());
			if (origin == null) {
				
				origin = new Point(route.getOrigin().getName(), route.getMapName());
				LOGGER.warn("MapServiceImpl->execute-> (Origin) This is a new point to save!");
				LOGGER.debug(String.format("MapServiceImpl->execute-> (Origin) This is a new point to save: %s", origin.toString()));
				pointRepository.save(origin);
			} else {
				
				LOGGER.warn("MapServiceImpl->execute-> (Origin) Found this point in the registers!");
				LOGGER.debug(String.format("MapServiceImpl->execute-> (Origin) Found this point in the registers: %s", origin.toString()));
			}
			
			//Save the route in the origin point.
			Route routeToSave = new Route(route.getMapName(), origin, destination, route.getDistance());
			origin.addRoute(routeToSave);
			LOGGER.warn("MapServiceImpl->execute-> (Route) Update the origin point with this route!");
			LOGGER.debug(String.format("MapServiceImpl->execute-> (Route) Update the origin point with this route: %s", origin.toString()));
			pointRepository.save(origin);

			LOGGER.warn("MapServiceImpl->execute-> ---------------------------------------------------------------------------------");
		}
		
		LOGGER.debug("MapService->execute->end()");
		return mapDomain;
	}
	
	private MapDto after(final Map mapDomain) {
		
		LOGGER.debug("MapServiceImpl->after->begin()");
		
		LOGGER.warn("MapServiceImpl->after-> Convert Domain Object <Map> to Data Transfer Object <MapDto>");
		MapDto mapDto = mapConvert.convertToDto(mapDomain);
		
		LOGGER.debug("MapServiceImpl->after->end()");
		return mapDto;
	}
}
