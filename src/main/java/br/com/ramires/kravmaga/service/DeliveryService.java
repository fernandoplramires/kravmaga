package br.com.ramires.kravmaga.service;

import br.com.ramires.kravmaga.dto.DeliveryDto;

public interface DeliveryService {
	
	public DeliveryDto run(DeliveryDto deliveryDto);
}
