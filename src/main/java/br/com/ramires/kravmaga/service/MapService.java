package br.com.ramires.kravmaga.service;

import br.com.ramires.kravmaga.dto.MapDto;

public interface MapService {
	
	public void run(MapDto mapDto);
}
