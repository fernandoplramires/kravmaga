package br.com.ramires.kravmaga;

import org.jsondoc.spring.boot.starter.EnableJSONDoc;
import org.neo4j.ogm.session.Session;
import org.neo4j.ogm.session.SessionFactory;

import org.springframework.context.annotation.*;
import org.springframework.data.neo4j.config.Neo4jConfiguration;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
import org.springframework.data.neo4j.server.Neo4jServer;
import org.springframework.data.neo4j.server.RemoteServer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("br.com.ramires.kravmaga")
@EnableAutoConfiguration
@EnableJSONDoc
@EnableNeo4jRepositories("br.com.ramires.kravmaga.repository")
public class Application extends Neo4jConfiguration {
	
	final String ENV_KRAVMAGA_DB;
	
	public Application() {
		
		ENV_KRAVMAGA_DB = System.getenv("ENV_KRAVMAGA_DB");
		
		if (ENV_KRAVMAGA_DB == null || ENV_KRAVMAGA_DB.length() == 0) {
			throw new RuntimeException("ENV ENV_KRAVMAGA_DB not available");
		}
		
		System.setProperty("username", ENV_KRAVMAGA_DB.substring(7, ENV_KRAVMAGA_DB.indexOf(":", 7)));
		System.setProperty("password", ENV_KRAVMAGA_DB.substring(ENV_KRAVMAGA_DB.indexOf(":", 7) + 1, ENV_KRAVMAGA_DB.indexOf("@")));
	}
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	@Override
	public Neo4jServer neo4jServer() {
		return new RemoteServer("http://" + ENV_KRAVMAGA_DB.substring(ENV_KRAVMAGA_DB.indexOf("@") + 1));
	}
	
	@Override
	public SessionFactory getSessionFactory() {
		return new SessionFactory("br.com.ramires.kravmaga.domain");
	}
	
	@Override
	@Bean
	@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
	public Session getSession() throws Exception {
		return super.getSession();
	}
}
