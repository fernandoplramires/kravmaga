package br.com.ramires.kravmaga.repository;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.stereotype.Repository;

import br.com.ramires.kravmaga.domain.Route;

@Repository
public interface RouteRepository extends GraphRepository<Route> {
	
	@Query("MATCH (org:POINT)-[rel:ROUTES]->(des:POINT) " +
		  " WHERE org.name = {0}                        " +
		  "   ADN des.name = {1}                        " +
		  "   AND rel.name = {2}                        " +
		  "   AND rel.map = {3}                         " +
		  "   AND rel.distance = {4}                    " +
		  "RETURN rel                                   " +
		  " LIMIT 1                                     " )
	public Route findByRouteDomain(String originName, String destinationName, String name, String map, double distance);
	
	/*
    @Query("MATCH p=(a:POINT{name:{0}})-[rel:ROUTES*..]->(b:POINT{name:{1}})          " +
        "   WHERE a.map = {2} AND b.map = {2}                                         " +
        "  RETURN p AS shortestPath,                                                  " +
        "         reduce(distance=0, r in rel | distance+r.distance) AS totalDistance " +
        "ORDER BY totalDistance ASC                                                   " +
        "   LIMIT 1                                                                   " )
    */
    @Query("MATCH (a:POINT{name:{0}})-[rel:ROUTES*..]->(b:POINT{name:{1}})            " +
        "   WHERE a.map = {2} AND b.map = {2}                                         " +
        "  RETURN rel AS shortestPath,                                                " +
        "         reduce(distance=0, r in rel | distance+r.distance) AS totalDistance " +
        "ORDER BY totalDistance ASC                                                   " +
       "   LIMIT 1                                                                   " )
	public Iterable<java.util.Map<String, Object>> getShortestWeightedPathByDijkstra(String originName, String destinationName, String mapName);
}
