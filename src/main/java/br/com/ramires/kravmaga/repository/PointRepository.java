package br.com.ramires.kravmaga.repository;

import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.stereotype.Repository;

import br.com.ramires.kravmaga.domain.Point;

@Repository
public interface PointRepository extends GraphRepository<Point> {
	
	public Point findByName(String pointName);
	
	public Point findByNameAndMap(String pointName, String mapName);
}
