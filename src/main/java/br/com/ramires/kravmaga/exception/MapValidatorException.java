package br.com.ramires.kravmaga.exception;

public class MapValidatorException extends RuntimeException {
	
	private static final long serialVersionUID = -7124521586639678592L;
	
	public MapValidatorException(final String message) {
        super(message);
    }
}
