package br.com.ramires.kravmaga.exception;

public class DeliveryDefaultException extends RuntimeException {
	
	private static final long serialVersionUID = 8897971862045756803L;
	
	public DeliveryDefaultException(final String message) {
        super(message);
    }
}