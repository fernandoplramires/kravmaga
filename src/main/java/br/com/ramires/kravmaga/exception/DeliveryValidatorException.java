package br.com.ramires.kravmaga.exception;

public class DeliveryValidatorException extends RuntimeException{
	
	private static final long serialVersionUID = 879697070752772556L;
	
	public DeliveryValidatorException(final String message) {
        super(message);
    }
}
