package br.com.ramires.kravmaga.exception;

public class MapDefaultException extends RuntimeException {
	
	private static final long serialVersionUID = -625385388267778062L;

	public MapDefaultException(final String message) {
        super(message);
    }
}
