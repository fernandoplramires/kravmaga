package br.com.ramires.kravmaga.exception;

public class DeliveryRepositoryException extends RuntimeException {
	
	private static final long serialVersionUID = -8158626260175280133L;

	public DeliveryRepositoryException(final String message) {
        super(message);
    }
}
