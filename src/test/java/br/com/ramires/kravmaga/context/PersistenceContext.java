/*
package br.com.ramires.kravmaga.context;

import org.neo4j.ogm.session.Session;
import org.neo4j.ogm.session.SessionFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.neo4j.config.Neo4jConfiguration;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
import org.springframework.data.neo4j.server.InProcessServer;
import org.springframework.data.neo4j.server.Neo4jServer;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableNeo4jRepositories("br.com.ramires.kravmaga.repository")
@EnableTransactionManagement
@ComponentScan("br.com.ramires.kravmaga")
@EnableAutoConfiguration
public class PersistenceContext extends Neo4jConfiguration {

	public static final int NEO4J_PORT = 7474;

	public PersistenceContext() {
		System.setProperty("username", "neo4j");
		System.setProperty("password", "ramires");
	}

	@Override
	public SessionFactory getSessionFactory() {
		return new SessionFactory("br.com.ramires.kravmaga.domain");
	}

	@Bean
	public Neo4jServer neo4jServer() {
		return new InProcessServer();
	}

	@Override
	@Bean
	public Session getSession() throws Exception {
		return super.getSession();
	}
}
*/