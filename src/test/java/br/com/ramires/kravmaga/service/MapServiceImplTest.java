package br.com.ramires.kravmaga.service;

import java.util.HashSet;
import java.util.Set;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.ramires.kravmaga.exception.MapDefaultException;

public class MapServiceImplTest {
	
	/*
	private static final Logger LOGGER = LoggerFactory.getLogger(MapServiceImplTest.class);
	
    @Rule
    public ExpectedException thrown = ExpectedException.none();
	
    MapService mapService;
	
	public MapServiceImplTest() { 
		mapService = new MapServiceImpl();
	}
	
	@Test  
	public void ShouldTrowMapDefaultExceptionForNullMap() {
        
		LOGGER.info("[TEST] MapServiceImplTest->ShouldTrowMapDefaultExceptionForNullMap->run()");
        thrown.expect(MapDefaultException.class);
        thrown.expectMessage("Map file is empty.");
		//thrown.expect(hasProperty("response", hasProperty("status", is(404))));
        
		Map mapRequest = null;
		
		mapService.run(mapRequest);
	}

	@Test
	public void ShouldTrowMapDefaultExceptionForNullMapMandatoryFields() {
		
		LOGGER.info("[TEST] MapServiceImplTest->ShouldTrowMapDefaultExceptionForNullMapMandatoryFields->run()");
        thrown.expect(MapDefaultException.class);
        thrown.expectMessage("Map mandatory fields are not correct inputed.");
        
		Map mapRequest = new Map();
		
		mapService.run(mapRequest);
	}
	
	@Test
	public void ShouldTrowMapDefaultExceptionForMandatoryFieldName() {
        
		LOGGER.info("[TEST] MapService->ShouldTrowMapDefaultExceptionForMandatoryFieldName->run()");
        thrown.expect(MapDefaultException.class);
        thrown.expectMessage("field name is empty.");
		
		Map mapRequest = new Map();
		mapRequest.setName("SP");
		
		Pointer origin = new Pointer();
		origin.setName("A");
		
		Pointer destination = new Pointer();
		destination.setName("B");
		
		Route route = new Route();
		route.setOrigin(origin);
		route.setDestination(destination);
		route.setDistance(10);
		
		Set<Route> routes = new HashSet<Route>();
		routes.add(route);
		mapRequest.setRoutes(routes);
		
		String name = "";
		mapRequest.setName(name);
		
		mapService.run(mapRequest);
	}
	
	@Test
	public void ShouldTrowMapDefaultExceptionForMandatoryFieldRoute() {
        
		LOGGER.info("[TEST] MapService->ShouldTrowMapDefaultExceptionForMandatoryFieldRoute->run()");
        thrown.expect(MapDefaultException.class);
        thrown.expectMessage("field route is empty.");
		
		Map mapRequest = new Map();
		mapRequest.setName("SP");
		
		mapService.run(mapRequest);
	}
	
	@Test
	public void ShouldInsertInTheDatabaseASimpleMap() {
		
		LOGGER.info("[TEST] MapService->ShouldInsertInTheDatabaseASimpleMap->run()");
		
		Set<Route> routes = new HashSet<Route>();
		for (int counter = 1; counter < 6; counter++) {
			
			Pointer origin = new Pointer();
			origin.setName("A"+counter);
			
			Pointer destination = new Pointer();
			destination.setName("B"+counter);
			
			Route route = new Route();
			route.setOrigin(origin);
			route.setDestination(destination);
			route.setDistance(counter);
			
			routes.add(route);
		}
		
		Map mapRequest = new Map();
		mapRequest.setName("TEST_MAP");
		mapRequest.setRoutes(routes);		
		LOGGER.info("[TEST] MapService->ShouldInsertInTheDatabaseASimpleMap->run(): mapRequest=[" + mapRequest + "]");

		mapService.run(mapRequest);
	}
	
	*/
}
