## **KravMaga** ##

A Java Webservice REST-API para roteamento e calculos envolvendo malhas de mapa.

### Arquitetura ###

● Maven

● Spring Boot

● Spring Data

● Spring Data Neo4J

● JSONDoc

### Dependencias ###

● Maven;

● Neo4J Database;

### Database configuration ###

● Necessario setar uma ENV "ENV_KRAVMAGA_DB" apontado as configuracoes do banco de dados, no formato: http://<usuario>:<senha>@<endereco>:<porta>

(por exemplo: http://neo4j:ramires@localhost:7474)

### Como executar ###

● Git clone do projeto;

● Entrar no diretorio do projeto;

● Executar atraves de um terminal: mvn clean spring-boot:run

● Utilizar a interface WEB do JSONDoc como referencia ou ate mesmo para enviar as solicitacoes que o sistema aceita.

### Documentacao ###

● Git clone do projeto;

● Entrar no diretorio do projeto;

● Executar atraves de um terminal: mvn clean spring-boot:run

● Abrir um browser com o seguinte endereco: http://localhost:8083/jsondoc-ui.html#

● Entrar com o valor no campo JSONDoc: http://localhost:8083/jsondoc

● Clicar em: "Get a Documentation"

### Motivacao ###

● Desenvolvi a solucacao utilizando um banco de dados nao relacional de grafos, o Neo4J. Banco na qual, hoje é o mais utilizado em solucoes que envolve grafos, empresas como Ebay e Walmart utilizam esta solução. Alem do motivo de mercado, outro motivo de tal utilizacao desta tecnologia, foi o aprendizado envolvendo um banco deste tipo, nao relacional, que ate o momento era novo para mim e o estudo de grafos, materia na qual considero muito interessante. 

### Duvidas ou Contato ###

● fernando.pl.ramires@gmail.com